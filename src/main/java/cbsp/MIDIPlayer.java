package cbsp;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

public abstract class MIDIPlayer extends Thread {

    protected Synthesizer synthesizer;
    protected MidiChannel channel;
    protected BlockingQueue<Note> buffer;

    MIDIPlayer(int playerChannel, int playerProgram) {

        // TASK 1, 2

        // Create blocking buffer.

        // Create MIDI Synthesizer

        // Set the current MIDI channel.

        // Set the current MIDI instrument.
    }

    public abstract void rcv();

    public void run() {

        Note t = null;

        System.out.println("PLAYER: START");

        this.rcv();

        while (true) {

            // TASK 1, 2

            // Get the latest Note from the buffer.

            // Play the Note.
        }
    }
}
